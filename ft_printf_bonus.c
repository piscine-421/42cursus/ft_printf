/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_bonus.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:16 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

void	chkprecision(const char *s, va_list l, int *i, t_flag *f)
{
	f->p = 0;
	*i += 1;
	if (pr_isdigit(s[*i]))
	{
		f->p = pr_atoi(&s[*i]);
		while (pr_isdigit(s[*i]))
			*i += 1;
	}
	else if (s[*i] == '*')
	{
		f->p = va_arg (l, int);
		*i += 1;
	}
	*i -= 1;
}

void	chkwidth(const char *s, va_list l, int *i, t_flag *f)
{
	if (pr_isdigit(s[*i]))
	{
		f->w = pr_atoi(&s[*i]);
		while (pr_isdigit(s[*i]))
			*i += 1;
		*i -= 1;
		return ;
	}
	f->w = va_arg (l, int);
	if (f->w >= 0)
		return ;
	f->w *= -1;
	f->d = 1;
}

void	chkflag(const char *s, va_list l, int *i, t_flag *f)
{
	if (s[*i] == '-')
		f->d = 1;
	else if (s[*i] == '0')
		f->z = 1;
	else if (s[*i] == '#')
		f->h = 1;
	else if (s[*i] == ' ')
		f->s = 1;
	else if (s[*i] == '+')
		f->c = 1;
	else if (pr_isdigit(s[*i]) || s[*i] == '*')
		chkwidth(s, l, i, f);
	else if (s[*i] == '.')
		chkprecision(s, l, i, f);
	else
		return ;
	*i += 1;
	chkflag(s, l, i, f);
}

int	prtfmt(const char *s, va_list l, int *i, t_flag *f)
{
	f->d = 0;
	f->z = 0;
	f->h = 0;
	f->s = 0;
	f->c = 0;
	f->w = -1;
	f->p = -1;
	chkflag(s, l, i, f);
	if (s[*i] == 'c')
		return (prtchr((char)va_arg (l, int), f));
	else if (s[*i] == 's')
		return (prtstr(va_arg (l, char *), f));
	else if (s[*i] == 'p')
		return (prtptr(va_arg (l, unsigned long), f));
	else if (s[*i] == 'd' || s[*i] == 'i')
		return (prtnbr(va_arg (l, int), f));
	else if (s[*i] == 'u')
		return (prtunbr(va_arg (l, unsigned int), f));
	else if (s[*i] == 'x')
		return (prthexl(va_arg (l, unsigned int), f));
	else if (s[*i] == 'X')
		return (prthexu(va_arg (l, unsigned int), f));
	return (prtchr(s[*i], f));
}

int	ft_printf(const char *format, ...)
{
	int		i;
	t_flag	f;
	va_list	l;

	f.r = 0;
	i = 0;
	if (!format)
		return (-1);
	va_start (l, format);
	while (format[i])
	{
		if (format[i] == '%')
		{
			i++;
			if (prtfmt(format, l, &i, &f) == -1)
				return (-1);
		}
		else if (pr_putchar_fd(format[i], 1, 1, &f) == -1)
			return (-1);
		i++;
	}
	va_end (l);
	return (f.r);
}
