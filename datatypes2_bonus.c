/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   datatypes2_bonus.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:03:59 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

int	prtnbr(long n, t_flag *f)
{
	if ((!f->d && (!f->z || f->p >= 0) && n < 0) || ((f->c || f->s) && n >= 0))
		f->w--;
	while (!f->d && (!f->z || f->p >= 0) && f->w >= g(n, 10, f) && f->w > f->p)
		if (pr_putchar_fd(' ', 1, 0, f) == -1)
			return (-1);
	if (f->c && n >= 0 && pr_putchar_fd('+', 0, 0, f) == -1)
		return (-1);
	if (!f->c && f->s && n >= 0 && pr_putchar_fd(' ', 0, 0, f) == -1)
		return (-1);
	if (n < 0)
	{
		if (pr_putchar_fd('-', 1, 0, f) == -1)
			return (-1);
		n *= -1;
	}
	while (f->p >= g(n, 10, f) || (!f->d && f->z && f->w >= g(n, 10, f)))
		if (pr_putchar_fd('0', 1, 1, f) == -1)
			return (-1);
	if ((n || f->p < 0) && pr_putnbr_fd(n, f) == -1)
		return (-1);
	while (f->d && f->w > 0)
		if (pr_putchar_fd(' ', 1, 1, f) == -1)
			return (-1);
	return (0);
}

int	prtunbr(unsigned int n, t_flag *f)
{
	while (!f->d && (!f->z || f->p >= 0) && f->w >= gu(n, 10, f) && f->w > f->p)
		if (pr_putchar_fd(' ', 1, 0, f) == -1)
			return (-1);
	while (f->p >= gu(n, 10, f) || (!f->d && f->z && f->w >= gu(n, 10, f)))
		if (pr_putchar_fd('0', 1, 1, f) == -1)
			return (-1);
	if ((n || f->p < 0) && pr_putnbr_fd(n, f) == -1)
		return (-1);
	while (f->d && f->w > 0)
		if (pr_putchar_fd(' ', 1, 1, f) == -1)
			return (-1);
	return (0);
}

int	prthexl(unsigned long n, t_flag *f)
{
	if (f->h && n)
		f->w -= 2;
	while (!f->d && (!f->z || f->p >= 0) && f->w >= gu(n, 16, f) && f->w > f->p)
		if (pr_putchar_fd(' ', 1, 0, f) == -1)
			return (-1);
	if (n && f->h)
	{
		if (pr_putchar_fd('0', 0, 0, f) == -1)
			return (-1);
		if (pr_putchar_fd('x', 0, 0, f) == -1)
			return (-1);
	}
	while (f->p >= gu(n, 16, f) || (!f->d && f->z && f->w >= gu(n, 16, f)))
		if (pr_putchar_fd('0', 1, 1, f) == -1)
			return (-1);
	if ((n || f->p < 0) && ft_putnbr_base(n, "0123456789abcdef", f) == -1)
		return (-1);
	while (f->d && f->w > 0)
		if (pr_putchar_fd(' ', 1, 1, f) == -1)
			return (-1);
	return (0);
}

int	prthexu(unsigned long n, t_flag *f)
{
	if (f->h && n)
		f->w -= 2;
	while (!f->d && (!f->z || f->p >= 0) && f->w >= gu(n, 16, f) && f->w > f->p)
		if (pr_putchar_fd(' ', 1, 0, f) == -1)
			return (-1);
	if (n && f->h)
	{
		if (pr_putchar_fd('0', 0, 0, f) == -1)
			return (-1);
		if (pr_putchar_fd('X', 0, 0, f) == -1)
			return (-1);
	}
	while (f->p >= gu(n, 16, f) || (!f->d && f->z && f->w >= gu(n, 16, f)))
		if (pr_putchar_fd('0', 1, 1, f) == -1)
			return (-1);
	if ((n || f->p < 0) && ft_putnbr_base(n, "0123456789ABCDEF", f) == -1)
		return (-1);
	while (f->d && f->w > 0)
		if (pr_putchar_fd(' ', 1, 1, f) == -1)
			return (-1);
	return (0);
}
