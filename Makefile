NAME		:= libftprintf.a
FLAGS		:= -Wextra -Wall -Werror -Wunreachable-code
SOURCES		:= datatypes.c datatypes2.c ft_printf.c libft.c
BONUS_SOURCES	:= $(SOURCES:.c=_bonus.c) libft2_bonus.c
OBJECTS		:= $(SOURCES:.c=.o)
BONUS_OBJECTS	:= $(BONUS_SOURCES:.c=.o)

all : $(SOURCES)
	cc $(FLAGS) -g -c $(SOURCES)
	ar rcs $(NAME) $(OBJECTS)

bonus : $(BONUS_SOURCES)
	cc $(FLAGS) -c $(BONUS_SOURCES)
	ar rcs $(NAME) $(BONUS_OBJECTS)

clean :
	rm -rf $(OBJECTS) $(BONUS_OBJECTS) $(NAME).dSYM

debug : $(BONUS_SOURCES)
	cc $(FLAGS) -g -c $(BONUS_SOURCES)
	ar rcs $(NAME) $(BONUS_OBJECTS)

fclean : clean
	rm -rf $(NAME) tester

$(NAME) : all

re : fclean all

test : bonus main.c
	cc $(FLAGS) -Wno-format libftprintf.a main.c -o tester
