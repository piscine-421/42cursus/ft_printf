/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:21 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	prtfmt(const char *s, va_list l, int *i, int *r)
{
	if (s[*i] == 'c')
		return (prtchr((char)va_arg (l, int), r));
	else if (s[*i] == 's')
		return (prtstr(va_arg (l, char *), r));
	else if (s[*i] == 'p')
		return (prtptr(va_arg (l, unsigned long), r));
	else if (s[*i] == 'd' || s[*i] == 'i')
		return (prtnbr(va_arg (l, int), r));
	else if (s[*i] == 'u')
		return (prtunbr(va_arg (l, unsigned int), r));
	else if (s[*i] == 'x')
		return (prthexl(va_arg (l, unsigned int), r));
	else if (s[*i] == 'X')
		return (prthexu(va_arg (l, unsigned int), r));
	return (prtchr(s[*i], r));
}

int	ft_printf(const char *format, ...)
{
	int		i;
	va_list	l;
	int		r;

	r = 0;
	i = 0;
	if (!format)
		return (-1);
	va_start (l, format);
	while (format[i])
	{
		if (format[i] == '%')
		{
			i++;
			if (prtfmt(format, l, &i, &r) == -1)
				return (-1);
		}
		else if (pr_putchar_fd(format[i], 1, &r) == -1)
			return (-1);
		i++;
	}
	va_end (l);
	return (r);
}
