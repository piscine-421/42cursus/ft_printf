/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:39 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"
#include <limits.h>
#include <stdio.h>

int	main(void)
{
	printf("Settings: \" %%%%\"\n");
	printf("printf:		");
	printf(" (%d)\n", printf(" %%"));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf(" %%"));
	printf("Settings: \"42%% 4.23d42\", 95519536\n");
	printf("printf:		");
	printf(" (%d)\n", printf("42% 4.23d42", 95519536));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("42% 4.23d42", 95519536));
	printf("Settings: \"!%% 69.29d!\", 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!% 69.29d!", 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!% 69.29d!", 0));
	printf("Settings: \"^.^/%% 52.36d^.^/\", -1750865640\n");
	printf("printf:		");
	printf(" (%d)\n", printf("^.^/% 52.36d^.^/", -1750865640));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("^.^/% 52.36d^.^/", -1750865640));
	printf("Settings: \"^.^/%% -56.23d^.^/\", 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("^.^/% -56.23d^.^/", 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("^.^/% -56.23d^.^/", 0));
	printf("Settings: \"!%% -32.28d!\", -169171921\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!% -32.28d!", -169171921));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!% -32.28d!", -169171921));
	printf("Settings: \" %%-2d \", -1\n");
	printf("printf:		");
	printf(" (%d)\n", printf(" %-2d ", -1));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf(" %-2d ", -1));
	printf("Settings: \"42%%+-33.6d42\", -1192026708\n");
	printf("printf:		");
	printf(" (%d)\n", printf("42%+-33.6d42", -1192026708));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("42%+-33.6d42", -1192026708));
	printf("Settings: \"!%% 27.2d!\", 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!% 27.2d!", 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!% 27.2d!", 0));
	printf("Settings: \"!%%-+1.14d!\", 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%-+1.14d!", 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%-+1.14d!", 0));
	printf("Settings: \"!%%#36.63x!\", -605172600\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%#36.63x!", -605172600));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%#36.63x!", -605172600));
	printf("Settings: \">------------<%%#23.5x>------------<\", -869505573\n");
	printf("printf:		");
	printf(" (%d)\n", printf(">------------<%#23.5x>------------<", -869505573));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf(">------------<%#23.5x>------------<", -869505573));
	printf("Settings: \"42%%#50.9x42\", 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("42%#50.9x42", 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("42%#50.9x42", 0));
	printf("Settings: \"!%%#0.1x!\", 971582320\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%#0.1x!", 971582320));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%#0.1x!", 971582320));
	printf("Settings: \"%%5p, %%5p, %%5p, %%5p, %%5p, %%5p\", (void *)0, (void *)0xABCDE, (void *)ULONG_MAX, (void *)LONG_MIN, (void *)-1, (void *)-2352\n");
	printf("printf:		");
	printf(" (%d)\n", printf("%5p, %5p, %5p, %5p, %5p, %5p", (void *)0, (void *)0xABCDE, (void *)ULONG_MAX, (void *)LONG_MIN, (void *)-1, (void *)-2352));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("%5p, %5p, %5p, %5p, %5p, %5p", (void *)0, (void *)0xABCDE, (void *)ULONG_MAX, (void *)LONG_MIN, (void *)-1, (void *)-2352));
	printf("Settings: 0\n");
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf(0));
	printf("Settings: \"%%0-20d\", -23523\n");
	printf("printf:		");
	printf(" (%d)\n", printf("%0-20d", 1423));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("%0-20d", 1423));
	printf("Settings: \"%%0 +#20.5c\", 'a'\n");
	printf("printf:		");
	printf(" (%d)\n", printf("%0 +#20.5c", 'a'));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("%0 +#20.5c", 'a'));
	printf("Settings: \"%%0- +#20.5c\", 'a'\n");
	printf("printf:		");
	printf(" (%d)\n", printf("%0- +#20.5c", 'a'));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("%0- +#20.5c", 'a'));
	printf("Settings: \"%%0 +#20.5%%\", 'a'\n");
	printf("printf:		");
	printf(" (%d)\n", printf("%0 +#20.5%"));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("%0 +#20.5%"));
	printf("Settings: \"%%0- +#20.5%%\"\n");
	printf("printf:		");
	printf(" (%d)\n", printf("%0- +#20.5%"));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("%0- +#20.5%"));
	printf("Settings: \"!%%00.0p!\", (void *)235\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%00.0p!", (void *)235));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%00.0p!", (void *)235));
	printf("Settings: \"!%%2.19p!\", (void *)235\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%2.19p!", (void *)235));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%2.19p!", (void *)235));
	printf("Settings: \"!%%wefsag!\", 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%wefsag!", 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%wefsag!", 0));
	printf("Settings: \"!%%#36#.63x!\", -605172600\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%36#.63x!", -605172600));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%36#.63x!", -605172600));
	printf("Settings: \"!%%#36.63.4x!\", -605172600\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%36#.63.4x!", -605172600));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%36#.63.4x!", -605172600));
	printf("Settings: \"!%%.#14wefsag!\", 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%.#14wefsag!", 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%.#14wefsag!", 0));
	printf("Settings: \"!%% *.*d!\", 27, 2, 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!% *.*d!", 27, 2, 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!% *.*d!", 27, 2, 0));
	printf("Settings: \"!%% **.*d!\", 27, 30, 2, 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!% **.*d!", 27, 30, 2, 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!% **.*d!", 27, 30, 2, 0));
	printf("Settings: \"!%%*.*d!\", -27, -2, 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%*.*d!", -27, -2, 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%*.*d!", -27, -2, 0));
	printf("Settings: \"!%%27.*d!\", 0, 0\n");
	printf("printf:		");
	printf(" (%d)\n", printf("!%27.*d!", -2, 0));
	ft_printf("ft_printf:	");
	ft_printf(" (%d)\n", ft_printf("!%27.*d!", -2, 0));
}
