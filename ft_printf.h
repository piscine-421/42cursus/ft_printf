/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:25 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdarg.h>
# include <unistd.h>

int		ft_printf(const char *format, ...);
int		ft_putnbr_base(unsigned long nbr, char *base, int *r);
int		pr_putchar_fd(char c, int fd, int *r);
int		pr_putstr_fd(char *s, int fd, int *r);
int		pr_putnbr_fd(unsigned int n, int fd, int *r);
long	pr_strlen(const char *s);
int		prtchr(char c, int *r);
int		prthexl(unsigned long n, int *r);
int		prthexu(unsigned long n, int *r);
int		prtnbr(long n, int *r);
int		prtptr(unsigned long n, int *r);
int		prtstr(char *s, int *r);
int		prtunbr(unsigned int n, int *r);

#endif
