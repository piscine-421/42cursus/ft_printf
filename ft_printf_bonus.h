/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf_bonus.h                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:19 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_BONUS_H
# define FT_PRINTF_BONUS_H
# include <stdarg.h>
# include <unistd.h>

typedef struct s_flag
{
	int	d;
	int	z;
	int	h;
	int	s;
	int	c;
	int	w;
	int	p;
	int	r;
}	t_flag;

int		ft_printf(const char *format, ...);
int		ft_putnbr_base(unsigned long nbr, char *b, t_flag *f);
int		g(long n, int base, t_flag *f);
int		gu(unsigned long n, int base, t_flag *f);
int		pr_atoi(const char *str);
int		pr_isdigit(int c);
int		pr_putchar_fd(char c, int l, int p, t_flag *f);
int		pr_putstr_fd(char *s, int l, int p, t_flag *f);
int		pr_putnbr_fd(unsigned int n, t_flag *f);
long	pr_strlen(const char *s);
int		prtchr(char c, t_flag *f);
int		prthexl(unsigned long n, t_flag *f);
int		prthexu(unsigned long n, t_flag *f);
int		prtnbr(long n, t_flag *f);
int		prtptr(unsigned long n, t_flag *f);
int		prtstr(char *s, t_flag *f);
int		prtunbr(unsigned int n, t_flag *f);

#endif
