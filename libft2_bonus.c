/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft2_bonus.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lcouturi <lcouturi@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/09/13 12:34:09 by lcouturi          #+#    #+#             */
/*   Updated: 2023/11/06 17:04:35 by lcouturi         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf_bonus.h"

int	pr_putstr_fd(char *s, int l, int p, t_flag *f)
{
	int	i;
	int	len;

	i = 0;
	len = pr_strlen(s);
	if (f->p >= 0 && len > f->p)
		len = f->p;
	while (s[i] && i < len)
	{
		if (pr_putchar_fd(s[i], l, p, f) == -1)
			return (-1);
		i++;
	}
	return (0);
}

long	pr_strlen(const char *s)
{
	int	i;

	i = 0;
	while (s && s[i] != '\0')
		i++;
	return (i);
}
